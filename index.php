<?php
//START A SESSION (JUST TO SAVE THE SELECTED EMISSION CHANNEL)
session_start();
if (!isset($_SESSION["eurl"])) {
	//DEFAULT EMISSION CHANNEL 1129
	$_SESSION["eurl"] = "http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1129&tvmode=horizontal";
}
?>

<!DOCTYPE html>
<html>

<head>
	<title>Prueba Ngine</title>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="styles.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="./xml2json.js"></script>
</head>

<body>
	<div id="myContainer" class="p-0 m-auto">
		<div class="row no-gutters h-100">
			<div class="col-9">
				<div id="wrapper" class="text-center"></div>
				<div class="text-center">
					<button class="btn btn-primary" id="videoButton">Video(I)</button>
					<button class="btn btn-primary" id="imageButton">Image(O)</button>
					<button class="btn btn-primary" id="apiButton">API(P)</button>
					<a class="btn btn-primary" onclick="showRemote()">Remote(Q)</a>
				</div>
			</div>
			<div class="col-3 border-left">
				<div id="apiContainerWrapper" class="d-inline-block">
					<pre id="apiContainer"></pre>
				</div>
			</div>
		</div>

	</div>
</body>

</html>

<script type="text/javascript">
	$(document).ready(function() {

		//STORAGE UPDATE TRIGGER
		window.onstorage = function(e) {
			//RELOAD WRAPPER
			$("#wrapper").load(window.location.href + "#wrapper");
		}

		//CHECK EVERY 5 SECONDS IF SERVER IS SENDING REQUEST ERROR: IF SO, DISABLE API BUTTON
		setInterval(() => {
			$.ajax({
				type: "GET",
				url: "http://www.ngine.co/test/api/index.php",
				headers: {
					'Access-Control-Allow-Origin': 'http://localhost:80',
					'Access-Control-Allow-Credentials': true,
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Methods': 'GET',
					'Access-Control-Allow-Headers': 'application/json'
				},
				complete: (r, s) => {
					console.log("status: " + s);

					if (s == "error") {
						$("#apiButton").prop("disabled", true);
					} else if (s == "success") {
						$("#apiButton").prop("disabled", false);
					} else {
						$("#apiButton").prop("disabled", false);
					}
				}
			});
		}, 5000);

		//HOTKEYS
		$(document).keypress((e) => {
			switch (e.key) {
				case "i":
					$.post("video.php", {}, (r) => {
						$("#wrapper").html(r);
					});
					break;
				case "o":
					$.post("image.php", {}, (r) => {
						$("#wrapper").html(r);
					});
					break;
				case "p":
					apiAction();
					break;
				case "q":
					showRemote();
					break;
			}
		});

		//DYNAMICALLY LOAD CONTENT OF:

		//EMISSION
		$.post("emission.php", {}, (r) => {
			$("#wrapper").html(r);
		});

		//VIDEO
		$("#videoButton").click(() => {
			$.post("video.php", {}, (r) => {
				$("#wrapper").html(r);
			});
		});

		//IMAGE
		$("#imageButton").click(() => {
			$.post("image.php", {}, (r) => {
				$("#wrapper").html(r);
			});
		});

		//API
		$("#apiButton").click(() => {
			apiAction();
		});

	});

	function showRemote() {
		window.open('remote.php',
			'newwindow',
			'width=500,height=500');
		return false;
	}

	function apiAction() {
		$.ajax({
			type: "GET",
			url: "http://www.ngine.co/test/api/index.php",
			headers: {
				'Access-Control-Allow-Origin': 'http://localhost:80',
				'Access-Control-Allow-Credentials': true,
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET',
				'Access-Control-Allow-Headers': 'application/json'
			},
			complete: (r, s) => {
				//GET CONTENT TYPE
				let cType = r.getResponseHeader("Content-type");
				//INIT. FINAL RESPONSE
				let finalResponse = "";

				//FORMAT DEPEDING ON THE RESPONSE TYPE
				switch (cType) {

					//IF SUCCESS
					case "application/xml":
						//CONVERT FROM XML TO JSON
						let conversion = xmlToJson(r.responseXML);
						//CONVERT TO JSON STRING
						finalResponse = JSON.stringify(conversion, undefined, 4);
						break;

						//IF PARSE ERROR
					case "application/json":
						//SET FINAL RESPONSE (THIS JSON IS MALFORMED)
						finalResponse = r.responseText;

						//CUTTING OFF ERRORS IN ORDER TO VALIDATE JSON
						let piece = finalResponse.slice(0, 747);
						let piece2 = finalResponse.slice(2636, 8179);
						let piece3 = finalResponse.slice(8180, finalResponse.length);

						//CUT THEM OFF OUR JSON TO EASE THE TASK
						let corps = finalResponse.slice(757, 1667);
						let corps_impression = finalResponse.slice(1707, 2617);

						//JOIN ALL REMAINING PARTS TO FORM A VALID JSON
						finalResponse = new String().concat(piece, piece2, piece3);

						//SANITIZE 'corps' & 'corps_impression' NODES AND INSERT THEM BACK INTO OUR JSON RESPONSE

						//REPLACE WRONG CHARACTERS BY THE DESIRED ONE (IN THIS CASE WE ARE GOING TO SWAP BETWEEN '<br>' HTML LINE BREAK AND '\n')
						corps = corps.replace(/<br>/g, "\n");
						corps_impression = corps_impression.replace(/<br>/g, "\n");

						//CONVERT STRING INTO JAVASCRIPT OBJECT IN ORDER TO INSERT NEW NODES
						realJSON = JSON.parse(finalResponse);

						//INSERT OUR NODES BACK
						realJSON.data.ad[0].corps = corps;
						realJSON.data.ad[0].corps_impression = corps_impression;

						//CONVERT INTO STRING AND OUTPUT THE RESPONSE
						finalResponse = JSON.stringify(realJSON, undefined, 4);

						break;
				}
				//RESPONSE DISPLAY
				$("#apiContainer").html(finalResponse);
			}
		});
	}
</script>