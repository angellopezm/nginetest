<header>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</header>

<div class="container p-2" id="remote">
    <div class="row no-gutters d-inline-block border rounded px-2 py-3">
        <div class="col-12 d-inline-block">
            <button id="btn-1" class="btn btn-primary d-block remoteButton" autofocus>WD_CH_1 (PRESS 1)</button>
            <button id="btn-2" class="btn btn-primary d-block remoteButton">WD_CH_2 (PRESS 2)</button>
            <button id="btn-3" class="btn btn-primary d-block remoteButton">WD_CH_3 (PRESS 3)</button>
        </div>
    </div>
</div>


<script type="text/javascript">

    //ON CLICK
    $("#btn-1").click(() => {
        postEmissionURL("http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1129&tvmode=horizontal");
        localStorage.setItem("ch", "1");
    });
    $("#btn-2").click(() => {
        postEmissionURL("http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1130&tvmode=horizontal");
        localStorage.setItem("ch", "2");
    });
    $("#btn-3").click(() => {
        postEmissionURL("http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1131&tvmode=horizontal");
        localStorage.setItem("ch", "3");
    });

    //HOTKEYS
    $(document).keypress((e) => {
        switch (e.key) {
            case "1":
                postEmissionURL("http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1129&tvmode=horizontal");
                localStorage.setItem("ch", "1");
                break;
            case "2":
                postEmissionURL("http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1130&tvmode=horizontal");
                localStorage.setItem("ch", "2");
                break;
            case "3":
                postEmissionURL("http://www.skysignage.com/s/front/channel_preview.php?ID_channel=1131&tvmode=horizontal");
                localStorage.setItem("ch", "3");
                break;
        }
    });

    function postEmissionURL(url = "") {
        $.post("emission.php", {
            url: url
        }, (r) => {
            $("#wrapper").html(r);
        });
    }
</script>